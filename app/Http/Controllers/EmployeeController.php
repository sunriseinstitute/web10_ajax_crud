<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Datatables;

class EmployeeController extends Controller
{
    public function index(Request $request){
        if ($request->ajax()) 
        {
            $data = DB::table('employees')->where('active', 1);
            if($request->gender != "all"){
                $data->where('gender', $request->gender);
            }
            return Datatables::of($data)
                ->addIndexColumn()
                ->addColumn('photo', function($row){
                    $photo = "<img src=". asset('assets/img/'.$row->photo) ." width='30px' height='30px' class='rounded-circle' />";
                    return $photo;
                })
                ->addColumn('action', function($row){
                    $btn_edit = "<button class='btn btn-warning' onclick='edit(". json_encode($row) .", this)'>Edit</button>";
                    $btn_delete = "<button class='btn btn-danger' onclick='showConfirm(". $row->id .", this)'>Delete</button>";
                    return $btn_edit. ' '. $btn_delete;
                })
                ->rawColumns(['action', 'photo'])
                ->make(true);
        }
        return view('employees.index');
    }

    public function save(Request $request){
        $data = [
            'name' => $request->name,
            'gender' => $request->gender,
            'dob' => $request->dob,
            'address' => $request->address
        ];

        // check if file uploaded
        if($request->hasFile('photo')){
            $photo = $request->file('photo'); // get image object
            $photo_name = strtotime("now"). '.' . $photo->getClientOriginalExtension(); // rename photo name and keep origin extesion
            $upload_destination = public_path('assets/img'); // where you store image in public folder.
            
            if($photo->move($upload_destination, $photo_name)){
                $data['photo'] = $photo_name;
            }
        }

        $res = DB::table('employees')->insertGetId($data);
        if($res){
            return response()->json([
                'status' => 200,
                'message' => 'success',
                'data' => $res
            ]);
        }else{
            return response()->json([
                'status' => 500,
                'message' => 'error',
                'data' => null
            ]);
        }
    }

    public function update(Request $request){
        $data = [
            'name' => $request->name,
            'gender' => $request->gender,
            'dob' => $request->dob,
            'address' => $request->address
        ];
        DB::table('employees')->where('id', $request->id)->update($data);
        return response()->json([
            'status' => 200,
            'message' => 'success',
            'data' => $data
        ]);
    }

    public function delete(Request $request){
        DB::table('employees')->where('id', $request->id)->update(['active' => 0]);
        return response()->json([
            'status' => 200,
            'message' => 'success',
            'data' => NULL
        ]);
    }
}
