@extends('layouts.master')

@section('content')
<section>
    <div class="row">
        <div class="col-sm"></div>
        <div class="col-sm">
            <form id="formSubmit" action="{{route('employee.save')}}" method="post">
                @csrf 
                <div class="form-group">
                    <label for="name">Name <span class="text-danger">*</span></label>
                    <input type="text" class="form-control" id="name" name="name" required>
                </div>
                <div class="form-group">
                    <label for="gender">Gender <span class="text-danger">*</span></label>
                    <select name="gender" id="gender" class="form-control" required>
                        <option value="">Select one</option>
                        <option value="1">Male</option>
                        <option value="2">Female</option>
                    </select>
                </div>
                <div class="form-group">
                    <label for="dob">Date of Birth <span class="text-danger">*</span></label>
                    <input type="date" class="form-control" id="dob" name="dob" required>
                </div>
                <div class="form-group">
                    <label for="address">Address</label>
                    <input type="text" class="form-control" id="address" name="address">
                </div>

                <div class="form-group mt-2">
                    <button id="btn_submit" class="btn btn-primary">Submit</button>
                </div>
            </form>
        </div>
        <div class="col-sm"></div>
    </div>
</section>
@endsection
@section('custom-js')
<script>
    $(document).ready(function(){
        $('#formSubmit').submit(function(e){
            e.preventDefault();
            
            $.ajax({
                type: 'post',
                url: "{{route('employee.save')}}",
                data: $('#formSubmit').serialize(),
                dataType: 'json',
                success: function(response){
                    console.log(response);
                }
            });
        });
    });
</script>

@endsection
