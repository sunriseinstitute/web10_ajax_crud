<!-- Modal -->
<div class="modal fade" id="editModal" tabindex="-1" aria-labelledby="createModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <form class="modal-content" id="editFormSubmit" action="{{route('employee.update')}}" method="post">
            @csrf
            <input type="text" name="id" id="eid">
            <div class="modal-header">
                <h5 class="modal-title" id="createModalLabel">Edit Employee</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">
                <div class="form-group">
                    <label for="name">Name <span class="text-danger">*</span></label>
                    <input type="text" class="form-control" id="ename" name="name" required>
                </div>
                <div class="form-group">
                    <label for="gender">Gender <span class="text-danger">*</span></label>
                    <select name="gender" id="egender" class="form-control" required>
                        <option value="">Select one</option>
                        <option value="1">Male</option>
                        <option value="2">Female</option>
                    </select>
                </div>
                <div class="form-group">
                    <label for="dob">Date of Birth <span class="text-danger">*</span></label>
                    <input type="date" class="form-control" id="edob" name="dob" required>
                </div>
                <div class="form-group">
                    <label for="address">Address</label>
                    <input type="text" class="form-control" id="eaddress" name="address">
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-bs-dismiss="modal" id="closeEditModal">Close</button>
                <button type="submit" class="btn btn-primary">Save Change</button>
            </div>
        </form>
    </div>
</div>
