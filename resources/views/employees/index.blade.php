@extends('layouts.master')

@section('content')
<div class="content px-3 pt-3 ">


    <div class="row">
        <div class="col-sm-1">
            <!-- Button create trigger modal -->
            <button type="button" 
                class="btn btn-primary mb-3" 
                data-bs-toggle="modal" 
                data-bs-target="#createModal">
                Create
            </button>
        </div>
        <div class="col-4"></div>
        <div class="col-sm-1">
            <select id="gender_id" class="form-control">
                <option value="all">ភេទ</option>
                <option value="1">ប្រុស</option>
                <option value="2">ស្រី</option>
            </select>
        </div>
        <div class="col-sm-1">
            <button class="btn btn-primary" id="btn_filter">Filter</button>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-12">
            <table class="table table-bordered w-100 datatable" id="dataTable">
                <thead>
                    <tr>
                        <th>id</th>
                        <th>No</th>
                        <th width="50">Picture</th>
                        <th>Name</th>
                        <th>Gender</th>
                        <th>DOB</th>
                        <th>Address</th>
                        <th>Actions</th>
                    </tr>
                </thead>
            </table>
        </div>
    </div>
</div>

@include('employees.create')
@include('employees.edit')
@include('employees.detele')

@endsection
@section('custom-js')
<script>
       $(document).ready(function () {
            //load data 
            // var gender = $('#gender_id').val();
            var table = $('#dataTable').DataTable({
                aaSorting: [[0,"desc"]],
                pageLength: 10,
                processing: true,
                serverSide: true,
                scrollX: true,
                ajax: {
                    url: "{{route('employee.index')}}",
                    data: function (d) {
                        d.gender = $('#gender_id').val();
                    }
                },
                columns: [
                    {
                        data: 'id', 
                        name: 'id', 
                        searchable: false, 
                        orderable: true,
                        visible: false,
                    }, 
                    {
                        data: 'DT_RowIndex', 
                        name: 'DT_RowIndex', 
                        searchable: false, 
                        orderable: false
                    }, 
                    {
                        data: 'photo',
                        name: 'photo',
                        orderable: false,
                        searchable: false
                    },
                    {data: 'name', name: 'employees.name'},
                    {
                        mData: function(row){
                            return row.gender == 1 ? 'Male' : 'Female';
                        },
                        orderable: false,
                        searchable: false, 
                    },
                    {data: 'dob', name: 'dob'},
                    {data: 'address', name: 'address'},
                    {
                        data: 'action',
                        name: 'action',
                        orderable: false,
                        searchable: false
                    }
                ],
            });

            $("#btn_filter").click(function(){
                table.draw(true);
            })

       });

</script>

<script>
    $(document).ready(function(){
        $('#formSubmit').submit(function(e){
            e.preventDefault();
            var formData = new FormData(this);
            $.ajax({
                type: 'post',
                url: "{{route('employee.save')}}",
                // data: $('#formSubmit').serialize(),
                data: formData,
                cache: false,
                contentType: false,
                processData: false,
                dataType: 'json',
                success: function(response){
                    if(response.status == 200){
                        console.log(response);
                        $('#dataTable').DataTable().ajax.reload();
                        $('#formSubmit')[0].reset();
                        $('#closeModal').trigger('click');
                        $('#successMessage').show();
                    }else{
                        alert("Error");
                    }
                }
            });
        });

        $('#editFormSubmit').submit(function(e){
            e.preventDefault();
           
            $.ajax({
                type: 'post',
                url: "{{route('employee.update')}}",
                data: $('#editFormSubmit').serialize(),
                dataType: 'json',
                success: function(response){
                    console.log(response)
                    if(response.status == 200){
                        $('#dataTable').DataTable().ajax.reload();
                        $('#editFormSubmit')[0].reset();
                        $('#closeEditModal').trigger('click');
                        $('#successMessage').show();
                    }else{
                        alert("Error");
                    }
                }
            });
        });

        // condfirm delete
        $("#confirmDelete").click(function(){
            var id =  $("#delete_id").val();
            remove(id);
        });


    });

    function edit(data){
        $('#editModal').modal('show');
        $('#eid').val(data.id)
        $('#ename').val(data.name)
        $('#egender').val(data.gender)
        $('#edob').val(data.dob)
        $('#eaddress').val(data.address)
    }

    function showConfirm(id){
        $("#delete_id").val(id);
        $('#confirmDeleteModal').modal('show');

    }

    function remove(id){
        $.ajax({
            type: 'post',
            url: "{{route('employee.delete')}}",
            data: {
                id: id,
                _token: "{{csrf_token()}}"
            },
            dataType: 'json',
            success: function(response){
                console.log(response)
                if(response.status == 200){
                    $('#dataTable').DataTable().ajax.reload();
                    // $('#editFormSubmit')[0].reset();
                    $('#closeConfirmDeleteModal').trigger('click');
                    $('#successMessage').show();
                }else{
                    alert("Error");
                }
            }
        });
    }
</script>
@endsection
