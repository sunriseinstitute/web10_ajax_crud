<!-- Modal -->
<div class="modal fade" id="confirmDeleteModal" tabindex="-1" aria-labelledby="confirmDeleteModalLabel" aria-hidden="true">
    <input type="hidden" id="delete_id">
    <div class="modal-dialog">
        <div class="modal-content" id="editFormSubmit">
            <div class="modal-header">
                <h4 class="modal-title" id="confirmDeleteModalLabel">Confirmation</h4>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-centent p-4">
                <h5>Are you sure to delete?</h5>
            </div>
 
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-bs-dismiss="modal" id="closeConfirmDeleteModal">No</button>
                <button type="button" id="confirmDelete" class="btn btn-primary">Yes</button>
            </div>
        </div>
    </div>
</div>
