@extends('layouts.master')

@section('content')
<!-- <div id="carouselExampleControls" class="carousel slide" data-bs-ride="carousel">
  <div class="carousel-inner">
    <div class="carousel-item active">
      <img src="{{ asset('assets/img/slide1.jpeg') }}" class="d-block w-100" alt="...">
    </div>
    <div class="carousel-item">
      <img src="{{ asset('assets/img/slide2.jpeg') }}" class="d-block w-100" alt="...">
    </div>
  </div>
  <button class="carousel-control-prev" type="button" data-bs-target="#carouselExampleControls" data-bs-slide="prev">
    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
    <span class="visually-hidden">Previous</span>
  </button>
  <button class="carousel-control-next" type="button" data-bs-target="#carouselExampleControls" data-bs-slide="next">
    <span class="carousel-control-next-icon" aria-hidden="true"></span>
    <span class="visually-hidden">Next</span>
  </button>
</div> -->
<section>
    <div class="row">
        <div class="col-sm"></div>
        <div class="col-sm">
            <form id="formSubmit" action="" method="post">
                @csrf 
                <div class="form-group">
                    <label for="name">Name <span class="text-danger">*</span></label>
                    <input type="text" class="form-control" id="name" name="name" required>
                </div>
                <div class="form-group">
                    <label for="gender">Gender <span class="text-danger">*</span></label>
                    <select name="gender" id="gender" class="form-control" required>
                        <option value="">Select one</option>
                        <option value="1">Male</option>
                        <option value="2">Female</option>
                    </select>
                </div>
                <div class="form-group">
                    <label for="dob">Date of Birth <span class="text-danger">*</span></label>
                    <input type="date" class="form-control" id="dob" name="dob" required>
                </div>
                <div class="form-group">
                    <label for="address">Address</label>
                    <input type="text" class="form-control" id="address" name="address">
                </div>

                <div class="form-group mt-2">
                    <button type="button" id="btn_submit" class="btn btn-primary">Submit</button>
                </div>
            </form>
        </div>
        <div class="col-sm"></div>
    </div>
</section>
@endsection
@section('custom-js')
<script>
    var myCarousel = document.querySelector('#carouselExampleControls')
        var carousel = new bootstrap.Carousel(myCarousel, {
        interval: 500,
        wrap: false
    })
</script>

<script>
    $(document).ready(function(){
        $('#btn_submit').click(function(){
            var name = $('#name').val();
            var gender = $('#gender').val();
            var dob = $('#dob').val();
            var address = $('#address').val();
            var token = "{{csrf_token()}}";
            $.ajax({
                type: 'post',
                url: "{{route('employee.save')}}",
                data:{
                    _token: token,
                    name: name,
                    gender: gender,
                    dob: dob,
                    address: address
                },
                // data:{
                //     name, gender, dob, address
                // }
                dataType: 'json',
                success: function(response){
                    console.log(response);
                }
            });
        });
    });
</script>

@endsection
