<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/form-data', function () {
    return view('formdata');
});

Route::get('employee', [App\Http\Controllers\EmployeeController::class, 'index'])->name('employee.index');
Route::post('employee/save', [App\Http\Controllers\EmployeeController::class, 'save'])->name('employee.save');
Route::post('employee/update', [App\Http\Controllers\EmployeeController::class, 'update'])->name('employee.update');
Route::post('employee/delete', [App\Http\Controllers\EmployeeController::class, 'delete'])->name('employee.delete');
