-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Apr 30, 2022 at 09:37 AM
-- Server version: 10.4.19-MariaDB
-- PHP Version: 7.4.20

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `web10_ajax`
--

-- --------------------------------------------------------

--
-- Table structure for table `employees`
--

CREATE TABLE `employees` (
  `id` int(11) NOT NULL,
  `name` varchar(200) NOT NULL,
  `gender` tinyint(4) NOT NULL COMMENT '1=Male; 2=Female',
  `dob` date NOT NULL,
  `address` varchar(200) DEFAULT NULL,
  `photo` varchar(200) DEFAULT 'default-profile.png',
  `active` tinyint(4) NOT NULL DEFAULT 1,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `employees`
--

INSERT INTO `employees` (`id`, `name`, `gender`, `dob`, `address`, `photo`, `active`, `created_at`, `updated_at`) VALUES
(1, 'Dara', 1, '2022-04-08', 'st. 2004, Sensok, Phnom Penh', 'default-profile.png', 1, '2022-04-02 08:37:23', NULL),
(2, 'Dara', 1, '2022-04-08', 'st. 2004, Sensok, Phnom Penh', 'default-profile.png', 1, '2022-04-02 08:37:48', NULL),
(3, 'testing', 1, '2022-04-02', 'st. 2004, Sensok, Phnom Penh', 'default-profile.png', 1, '2022-04-02 09:01:07', NULL),
(4, 'Rithy SAM', 1, '2022-04-28', 'ផ្ទះលេខ​259A ផ្លូវ2002 សង្កាត់ទឹកថ្លា ខណ្ឌសែខសុខ រាជធានីភ្នំពេញ', 'default-profile.png', 1, '2022-04-02 09:03:09', NULL),
(5, 'Rithy SAM', 1, '2022-04-28', 'ផ្ទះលេខ​259A ផ្លូវ2002 សង្កាត់ទឹកថ្លា ខណ្ឌសែខសុខ រាជធានីភ្នំពេញ', 'default-profile.png', 1, '2022-04-02 09:03:18', NULL),
(6, 'Rithy SAM', 1, '2022-04-28', 'ផ្ទះលេខ​259A ផ្លូវ2002 សង្កាត់ទឹកថ្លា ខណ្ឌសែខសុខ រាជធានីភ្នំពេញ', 'default-profile.png', 1, '2022-04-02 09:03:47', NULL),
(7, 'Rithy SAM', 1, '2022-04-28', 'ផ្ទះលេខ​259A ផ្លូវ2002 សង្កាត់ទឹកថ្លា ខណ្ឌសែខសុខ រាជធានីភ្នំពេញ', 'default-profile.png', 1, '2022-04-02 09:03:47', NULL),
(8, 'Rithy SAM', 1, '2022-04-28', 'ផ្ទះលេខ​259A ផ្លូវ2002 សង្កាត់ទឹកថ្លា ខណ្ឌសែខសុខ រាជធានីភ្នំពេញ', 'default-profile.png', 1, '2022-04-02 09:03:47', NULL),
(9, 'Rithy SAM', 1, '2022-04-28', 'ផ្ទះលេខ​259A ផ្លូវ2002 សង្កាត់ទឹកថ្លា ខណ្ឌសែខសុខ រាជធានីភ្នំពេញ', 'default-profile.png', 1, '2022-04-02 09:03:48', NULL),
(10, 'Rithy SAM', 1, '2022-04-28', 'ផ្ទះលេខ​259A ផ្លូវ2002 សង្កាត់ទឹកថ្លា ខណ្ឌសែខសុខ រាជធានីភ្នំពេញ', 'default-profile.png', 1, '2022-04-02 09:03:48', NULL),
(11, 'testing', 1, '2022-04-04', 'st. 2004, Sensok, Phnom Penh', 'default-profile.png', 1, '2022-04-03 07:32:25', NULL),
(12, 'testing', 1, '2022-04-04', 'st. 2004, Sensok, Phnom Penh', 'default-profile.png', 1, '2022-04-03 07:32:27', NULL),
(13, 'testing', 1, '2022-04-04', 'st. 2004, Sensok, Phnom Penh', 'default-profile.png', 1, '2022-04-03 07:32:27', NULL),
(14, 'testing', 1, '2022-04-04', 'st. 2004, Sensok, Phnom Penh', 'default-profile.png', 1, '2022-04-03 07:32:27', NULL),
(15, 'testing', 1, '2022-04-04', 'st. 2004, Sensok, Phnom Penh', 'default-profile.png', 1, '2022-04-03 07:32:36', NULL),
(16, 'testing', 1, '2022-04-04', 'st. 2004, Sensok, Phnom Penh', 'default-profile.png', 1, '2022-04-03 07:32:37', NULL),
(17, 'testing', 1, '2022-04-04', 'st. 2004, Sensok, Phnom Penh', 'default-profile.png', 1, '2022-04-03 07:32:42', NULL),
(18, 'Rithy', 1, '2022-04-06', 'ផ្ទះលេខ​259A ផ្លូវ2002 សង្កាត់ទឹកថ្លា ខណ្ឌសែខសុខ រាជធានីភ្នំពេញ', 'default-profile.png', 1, '2022-04-03 08:04:06', NULL),
(19, 'Dara', 1, '2022-04-07', NULL, 'default-profile.png', 1, '2022-04-03 08:43:17', NULL),
(20, 'testing', 2, '2022-04-12', 'tesgfs', 'default-profile.png', 1, '2022-04-03 08:49:21', NULL),
(21, 'testing', 2, '2022-04-12', 'tesgfs', 'default-profile.png', 1, '2022-04-03 08:49:23', NULL),
(22, 'testing', 2, '2022-04-12', 'tesgfs', 'default-profile.png', 1, '2022-04-03 08:49:23', NULL),
(23, 'testing', 2, '2022-04-12', 'tesgfs', 'default-profile.png', 1, '2022-04-03 08:49:23', NULL),
(24, 'testing', 2, '2022-04-12', 'tesgfs', 'default-profile.png', 1, '2022-04-03 08:49:23', NULL),
(25, 'testing', 2, '2022-04-12', 'tesgfs', 'default-profile.png', 1, '2022-04-03 08:49:23', NULL),
(26, 'testing', 2, '2022-04-12', 'tesgfs', 'default-profile.png', 1, '2022-04-03 08:49:23', NULL),
(27, 'Dara', 1, '2022-04-06', NULL, 'default-profile.png', 1, '2022-04-03 08:51:18', NULL),
(28, 'testing', 1, '2022-04-21', 'stfsf', 'default-profile.png', 1, '2022-04-03 08:52:32', NULL),
(29, 'fsf', 1, '2022-03-31', 'sfsf', 'default-profile.png', 1, '2022-04-03 08:54:05', NULL),
(30, 'testing', 1, '2022-04-07', 'sfsf', 'default-profile.png', 1, '2022-04-03 08:55:20', NULL),
(31, 'testing', 2, '2022-04-14', NULL, 'default-profile.png', 1, '2022-04-03 08:58:07', NULL),
(32, 'testing', 1, '2022-04-06', NULL, 'default-profile.png', 1, '2022-04-03 08:58:38', NULL),
(33, 'testing', 1, '2022-04-12', 'stsf', 'default-profile.png', 1, '2022-04-03 09:00:59', NULL),
(34, 'testing', 1, '2022-04-21', NULL, 'default-profile.png', 1, '2022-04-03 09:01:50', NULL),
(35, 'testing', 1, '2022-04-14', 'ផ្ទះលេខ​259A ផ្លូវ2002 សង្កាត់ទឹកថ្លា ខណ្ឌសែខសុខ រាជធានីភ្នំពេញ', 'default-profile.png', 1, '2022-04-03 09:02:05', NULL),
(36, 'testing', 1, '2022-04-13', NULL, 'default-profile.png', 1, '2022-04-03 09:02:19', NULL),
(37, 'Dara', 1, '2022-04-06', NULL, 'default-profile.png', 1, '2022-04-03 09:02:32', NULL),
(38, 'Dara', 1, '2022-04-13', NULL, 'default-profile.png', 1, '2022-04-03 09:02:57', NULL),
(39, 'testing', 1, '2022-04-19', NULL, 'default-profile.png', 1, '2022-04-03 09:08:08', NULL),
(40, 'Dara', 1, '2022-04-13', NULL, 'default-profile.png', 1, '2022-04-03 09:16:21', NULL),
(41, 'testing', 2, '2022-04-06', NULL, 'default-profile.png', 1, '2022-04-03 09:17:16', NULL),
(42, 'Dara', 2, '2022-04-06', NULL, 'default-profile.png', 1, '2022-04-03 09:17:47', NULL),
(43, 'Dara', 2, '2022-04-26', NULL, 'default-profile.png', 1, '2022-04-03 09:18:03', NULL),
(44, 'testing', 1, '2022-04-14', NULL, 'default-profile.png', 1, '2022-04-03 09:18:23', NULL),
(45, 'Dara', 1, '2022-04-12', NULL, 'default-profile.png', 1, '2022-04-03 09:20:55', NULL),
(46, 'Dara', 1, '2022-04-06', NULL, 'default-profile.png', 1, '2022-04-03 09:21:11', NULL),
(47, 'Dara', 2, '2022-04-19', NULL, 'default-profile.png', 1, '2022-04-03 09:21:19', NULL),
(48, 'Dara', 1, '2022-04-13', NULL, 'default-profile.png', 1, '2022-04-03 09:24:11', NULL),
(49, 'Dara', 2, '2022-04-06', NULL, 'default-profile.png', 1, '2022-04-03 09:24:19', NULL),
(50, 'hello', 1, '2022-04-04', NULL, 'default-profile.png', 1, '2022-04-03 09:28:27', NULL),
(51, 'testing', 1, '2022-04-12', NULL, 'default-profile.png', 1, '2022-04-03 09:33:04', NULL),
(52, 'testing', 1, '2022-04-12', NULL, 'default-profile.png', 1, '2022-04-03 09:33:05', NULL),
(53, 'testing', 1, '2022-04-12', NULL, 'default-profile.png', 1, '2022-04-03 09:33:06', NULL),
(54, 'testing', 1, '2022-04-12', NULL, 'default-profile.png', 1, '2022-04-03 09:33:06', NULL),
(55, 'testing', 1, '2022-04-12', NULL, 'default-profile.png', 1, '2022-04-03 09:33:06', NULL),
(56, 'testing', 1, '2022-04-12', NULL, 'default-profile.png', 1, '2022-04-03 09:33:06', NULL),
(57, 'testing', 1, '2022-04-12', NULL, 'default-profile.png', 0, '2022-04-03 09:33:07', '2022-04-09 02:35:05'),
(58, 'testing', 1, '2022-04-12', NULL, 'default-profile.png', 0, '2022-04-03 09:33:07', '2022-04-09 02:35:01'),
(59, 'Dara123', 1, '2022-04-05', NULL, 'default-profile.png', 0, '2022-04-03 09:34:16', '2022-04-09 02:34:59'),
(60, 'testing143', 1, '2022-03-30', NULL, 'default-profile.png', 0, '2022-04-03 09:36:08', '2022-04-09 02:23:48'),
(61, 'testing123', 1, '2022-04-06', NULL, 'default-profile.png', 0, '2022-04-03 09:36:46', '2022-04-09 02:23:41'),
(62, 'Dara1349493', 2, '2022-04-06', NULL, 'default-profile.png', 0, '2022-04-03 09:37:01', '2022-04-09 02:34:53'),
(63, 'Proile test', 1, '2022-04-13', 'st. 2004, Sensok, Phnom Penh', 'default-profile.png', 1, '2022-04-12 03:12:08', NULL),
(64, 'testing', 1, '2022-04-13', 'ផ្ទះលេខ​259A ផ្លូវ2002 សង្កាត់ទឹកថ្លា ខណ្ឌសែខសុខ រាជធានីភ្នំពេញ', 'default-profile.png', 1, '2022-04-12 03:20:46', NULL),
(65, 'Dara', 1, '2022-04-13', '150 sensok', '1649734401.png', 1, '2022-04-12 03:33:21', NULL),
(66, 'Dara', 1, '2022-04-21', '#9BE0, Street 86 Sangkat Srash Chark, Khan Daun Penh, Phnom Penh, Cambodia.', '1649734421.webp', 1, '2022-04-12 03:33:41', NULL),
(67, 'Dara', 1, '2022-04-21', NULL, '1649734485.png', 1, '2022-04-12 03:34:45', NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `employees`
--
ALTER TABLE `employees`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `employees`
--
ALTER TABLE `employees`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=68;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
